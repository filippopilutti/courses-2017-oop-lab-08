package it.unibo.oop.lab.advanced;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 */
public class DrawNumberApp implements DrawNumberViewObserver {
	
	private static final String FILE_NAME = System.getProperty("user.dir")
											+ System.getProperty("file.separator")
											+ "res" + System.getProperty("file.separator")
											+ "config.yml";
	
    private int min;
    private int max;
    private int attempts;
    private final DrawNumber model;
    private final DrawNumberView view;

    /**
     * @throws IOException 
     * 
     */
    public DrawNumberApp() throws IOException {
    	
    	BufferedReader br = new BufferedReader(new FileReader(FILE_NAME));
    	
    	StringTokenizer st = new StringTokenizer(br.readLine());
    	st.nextToken();
    	this.min = Integer.parseInt(st.nextToken());
    	
    	st = new StringTokenizer(br.readLine());
    	st.nextToken();
    	this.max = Integer.parseInt(st.nextToken());
    	
    	st = new StringTokenizer(br.readLine());
    	st.nextToken();
    	this.attempts = Integer.parseInt(st.nextToken());
    	
    	br.close();
    	
        this.model = new DrawNumberImpl(min, max, attempts);
        this.view = new DrawNumberViewImpl();
        this.view.setObserver(this);
        this.view.start();
    }

    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            this.view.result(result);
        } catch (IllegalArgumentException e) {
            this.view.numberIncorrect();
        } catch (AttemptsLimitReachedException e) {
            view.limitsReached();
        }
    }

    @Override
    public void resetGame() {
        this.model.reset();
    }

    @Override
    public void quit() {
        System.exit(0);
    }

    /**
     * @param args
     *            ignored
     * @throws IOException 
     */
    public static void main(final String... args) throws IOException {
        new DrawNumberApp();
    }

}
