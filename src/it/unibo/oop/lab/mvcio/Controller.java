package it.unibo.oop.lab.mvcio;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * 
 */
public class Controller {
	
	private final static String DEFAULT_PATH = System.getProperty("user.home")
												+ System.getProperty("file.separator")
													+ "output.dat";
	
	private File file;
	
	public Controller() {
		this.file = new File(DEFAULT_PATH);
	}

    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class in such a way that:
     * 
     * 1) It has a method for setting a File as current file
     * 
     * 2) It has a method for getting the current File
     * 
     * 3) It has a method for getting the path (in form of String) of the current
     * File
     * 
     * 4) It has a method that gets a Serializable as input and saves such Object in
     * the current file. Remember how to use the ObjectOutputStream. This method may
     * throw IOException.
     * 
     * 5) By default, the current file is "output.dat" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that run correctly on every platform.
     */
	
	public void setCurrentFile(final String filePath){
		this.file = new File(filePath);
	}
	
	public void setCurrentFile(final File file) {
		this.file = file;
	}
	
	public File getCurrentFile() {
		return this.file;
	}
	
	public String getFilePath() {
		return System.getProperty("user.home")
				+ System.getProperty("file.separator")
				+ this.file.getName();
	}
	
	public void serialize(final Serializable ser) throws IOException, ClassNotFoundException {
		final ObjectOutputStream ostream = new ObjectOutputStream(
				new BufferedOutputStream(
						new FileOutputStream(this.getFilePath())));
		ostream.writeObject(ser);
		ostream.close();
		
	}
}
