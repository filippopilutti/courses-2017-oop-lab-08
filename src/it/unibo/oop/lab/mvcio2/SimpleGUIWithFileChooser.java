package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import it.unibo.oop.lab.mvcio.Controller;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {
	
	private final static String PATH = System.getProperty("user.home")
										+ System.getProperty("file.separator");
	
	private final JFrame frame = new JFrame("File Controller");
	
    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */
	
	public SimpleGUIWithFileChooser() {
		/**
		 * View setup
		 */
		final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        frame.setLocationByPlatform(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
        /**
         * Main Panel:
         * 	-secondary panel;
         * 	-save button;
         * 	-text area.
         */
        final JPanel pan = new JPanel();
        pan.setLayout(new BorderLayout());
        
        final JButton save = new JButton("Salva");
        pan.add(save, BorderLayout.SOUTH);
        final JTextArea t = new JTextArea();
        pan.add(t);
        Controller ctrl = new Controller();
        save.addActionListener(new ActionListener() {
        	public void actionPerformed(final ActionEvent e) {
        		final String text = t.getText();
        		try {
					ctrl.serialize(text);
				} catch (IOException | ClassNotFoundException e1) {
					e1.printStackTrace();
				}
        	}
        });
        
        /**
         * Secondary panel:
         * 	-text field;
         *  -browse button.
         */
        final JPanel pan2 = new JPanel();
        pan2.setLayout(new BorderLayout());
        final JTextField t2 = new JTextField(ctrl.getFilePath());
        t2.setEditable(false);
        pan2.add(t2, BorderLayout.CENTER);
        final JButton browse = new JButton("Browse");
        pan2.add(browse, BorderLayout.AFTER_LINE_ENDS);
        browse.addActionListener(new ActionListener() {
        	public void actionPerformed(final ActionEvent e) {
        		JFileChooser ch = new JFileChooser(PATH);
        		int i = ch.showSaveDialog(null);
        		final File file = ch.getSelectedFile();
        		if (i == JFileChooser.APPROVE_OPTION) {
        			ctrl.setCurrentFile(file);
        			t2.setText(ctrl.getFilePath());
        		} else if (i == JFileChooser.CANCEL_OPTION) {
        			
        		} else {
        			JOptionPane.showMessageDialog(null, "Error");
        		}
        	}
        });
        
        pan.add(pan2, BorderLayout.NORTH);
        frame.add(pan);
        frame.setVisible(true);
	}
	
	public static void main(String... args) {
		new SimpleGUIWithFileChooser();
	}
}
