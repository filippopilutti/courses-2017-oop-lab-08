package it.unibo.oop.lab.mvc;


import java.util.ArrayList;
import java.util.List;

public class Mycontroller implements Controller{
	
	private String current;
	private List<String> history;
	
	public Mycontroller() {
		this.current = null;
		this.history = new ArrayList<>();
	}
	
	@Override
	public void setString(String s) throws NullPointerException {
		if (s == null) {
			throw new NullPointerException();
		} else {
			this.current = s;
		}
	}

	@Override
	public String getString() {
		return this.current;
	}

	@Override
	public List<String> getHistory() {
		return this.history;
	}

	@Override
	public void printString() throws IllegalStateException {
		if (this.current == null) {
			throw new IllegalStateException();
		} else {
			this.history.add(this.current);
			System.out.println(this.current);
		}
	}

}
